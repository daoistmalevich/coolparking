﻿using CoolParking.BL.Models;
using CoolParking.BL.Services;
using System;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;

namespace CoolParking.PL
{
    public class Program
    {
        private static ParkingService _parkingService;
        private static string _path = $@"{Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)}\Transactions.log";

        static void Main(string[] args)
        {
            var payTimer = new TimerService();
            var logTimer = new TimerService();
            var logService = new LogService(_path);

            _parkingService = new ParkingService(payTimer, logTimer, logService);
            bool showMenu = true;
            while (showMenu)
            {
                showMenu = MainMenu();
            }
        }
        private static bool MainMenu()
        {
            Console.Clear();
            Console.WriteLine("Choose an option:");
            Console.WriteLine("1) Get Parking Balance");
            Console.WriteLine("2) Get Sum Of Earned Maney");
            Console.WriteLine("3) Show Number Of Free Parking Places");
            Console.WriteLine("4) Show All Transaction");
            Console.WriteLine("5) Show Transaction Log");
            Console.WriteLine("6) Show List Of Parked Vehicles");
            Console.WriteLine("7) Put A Vehicle On Parking");
            Console.WriteLine("8) Remove A Vehicle From Parking");
            Console.WriteLine("9) Top Up The Vehicle Balance");
            Console.WriteLine("0) Exit");
            Console.Write("\r\nSelect an option: ");

            switch (Console.ReadLine())
            {
                case "1":
                    Console.WriteLine("\r\nYou have choosen To: Get Parking Balance");
                    Console.WriteLine($"\r\nCurrent Parking Balance is: {_parkingService.GetBalance()}");
                    Console.Write("\r\nPress Enter to return to Main Menu");
                    return true;
                case "2":
                    Console.WriteLine("\r\nYou have choosen To: Get Sum Of Earned Maney");
                    Console.WriteLine($"\r\nCurrent Parking Balance is: {_parkingService.GetAllSum()}");
                    return true;
                case "3":
                    Console.WriteLine("\r\nYou have choosen To: Show Number Of Free Parking Places");
                    Console.WriteLine($"\r\nCurrent Number Free Parking Places is: {_parkingService.GetFreePlaces()}");

                    return true;
                case "4":
                    Console.WriteLine("\r\nYou have choosen To: Show All Transaction");
                    Console.WriteLine($"\r\nCurrent Transactions is: {_parkingService.GetLastParkingTransactions()}");
                    return true;
                case "5":
                    Console.WriteLine("\r\nYou have choosen To: Show Transaction Log");
                    Console.WriteLine($"\r\nTransaction Log is: {_parkingService.ReadFromLog()}");
                    return true;
                case "6":
                    Console.WriteLine("\r\nYou have choosen To: Show List Of Parked Vehicles");
                    Console.WriteLine($"\r\nList Of Parked Vehicles is: {_parkingService.GetVehicles()}");
                    return true;
                case "7":
                    return SetVehicleToAdd();
                case "8":
                    return SetVehicleIdToRemove();
                case "9":
                    return TopUpCurrentVehicle();
                case "0":
                    Console.WriteLine("\r\nYou have choosen To: Exit");

                    return false;
                default:
                    return true;
            }

        }
        private static bool SetVehicleToAdd()
        {
            Console.WriteLine("\r\nYou have choosen To: Put A Vehicle On Parking");
            Console.WriteLine("\r\nTo Put Vehicle You Need To Register Your Vehicle");
            var id = GetVehicleId();
            Console.WriteLine("\r\nChoose Your Vehicle Type (0 - PassengerCar, 1 - Truck, 2 - Bus, 3 - Motorcycle) :");

            var type = Console.ReadLine();
            VehicleType vehicle;
            while (!Enum.TryParse(type, false, out vehicle))
            {
                Console.WriteLine("Invalid Vehicle Type, Write correct :");
                type = Console.ReadLine();
            }
            Console.WriteLine("\r\nType Amount Of Balance On Your Vehicle (Should be > 0) : ");
            string curSum = Console.ReadLine();
            int balance;
            while (!int.TryParse(curSum, out balance) || balance <= 0)
            {
                Console.WriteLine("Invalid Balance Format, Write correct one :");
                curSum = Console.ReadLine();
            }
            _parkingService.AddVehicle(new Vehicle(id, vehicle, balance));

            Console.WriteLine($"\r\nThe Vehicle with parameters {id}, {vehicle}, {balance} is Parked");
            return false;
        }
        private static bool SetVehicleIdToRemove()
        {
            Console.WriteLine("\r\nYou have choosen To: Remove A Vehicle From Parking");
            Console.WriteLine("\r\nTo Remove Vehicle You Need To Write Your Vehicle Id");
            var id = GetVehicleId();
            if (_parkingService.GetVehicles().FirstOrDefault(dmr => dmr.Id == id).Balance < 0)
            {
                Console.WriteLine("You Cannot Remove Vehicle Until You Pay Your Dept");
                return false;  //Чи вийде з блоку кейс чи ні?
            }
            _parkingService.RemoveVehicle(id);
            Console.WriteLine($"\r\nThe Vehicle with id {id} is Removed From Parking");
            return true;
        }
        private static bool TopUpCurrentVehicle()
        {
            Console.WriteLine("\r\nYou have choosen To: Top Up The Vehicle Balance");
            Console.WriteLine("\r\nTo Top Up Vehicle You Need To Write Your Vehicle Id");
            var id = GetVehicleId();
            Console.WriteLine("\r\nType Amount Of Cash To Top Up Your Vehicle (Should be > 0) : ");
            string topUpBalance = Console.ReadLine();
            int balanceTopUp;
            while (!int.TryParse(topUpBalance, out balanceTopUp) || balanceTopUp < 0)
            {
                Console.WriteLine("Invalid Amount Of Cash , Write correct one :");
                topUpBalance = Console.ReadLine();
            }
            _parkingService.TopUpVehicle(id, balanceTopUp);
            Console.WriteLine($"\r\nThe Vehicle with id {id} is top uped on {balanceTopUp} Sum");
            return true;
        }

        private static string GetVehicleId()
        {
            Console.WriteLine("\r\nType Vehicle Id (Should have format \"XX-YYYY-XX\" Where X - Character, Y - Number) : ");
            string plate = Console.ReadLine();
            while (!IsMatched(plate))
            {
                Console.WriteLine("Invalid Vehicle Id Format, Write correct one :");
                plate = Console.ReadLine();
            }
            return plate;
        }
        private static bool IsMatched(string plate)
        {
            string _pattern = @"^([A-Z]{2}\-\d{4}\-[A-Z]{2}$)";
            return Regex.IsMatch(plate, _pattern);
        }
    }
}
