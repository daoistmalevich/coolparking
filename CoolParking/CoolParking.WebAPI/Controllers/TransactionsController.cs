﻿using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;

namespace CoolParking.WebAPI.Controllers
{
    public class TransactionsController : BaseController
    {
        public TransactionsController(IParkingService service, ITimerService payTimer, ITimerService logTimer, ILogService logService) : base(service, payTimer, logTimer, logService)
        {
        }

        //GET api/transactions/last
        //[Route("~/transactions")]
        [HttpGet("last")]
        //[Route("~/last")]
        public ActionResult<TransactionInfo[]> GetLastTransactions()
        {
            try
            {
                return Ok(_parkingService.GetLastParkingTransactions());
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        //GET api/transactions/all
        //[Route("~/transactions")]
        [HttpGet("all")]
        //[Route("~/all")]
        public ActionResult<string> GetAllTransactions()
        {
            try
            {
                return Ok(_parkingService.ReadFromLog());
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

        }

        //PUT api/transactions/topUpVehicle
        //[Route("~/transactions")]
        [HttpPut("topUpVehicle")]
        public ActionResult TopUpVehicleBalance(string id, decimal sum)
        {
            var vehicle = _parkingService.GetVehicles().FirstOrDefault(dm => dm.Id == id);
            try
            {
                if (!vehicle.IsMatched(id))
                    return BadRequest();
                if (vehicle == null)
                    return NotFound();
                _parkingService.TopUpVehicle(id, sum);
                return Ok(vehicle);
            }
            catch(Exception ex)
            {
                return NotFound();
            }
        }
    }
}
