﻿using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace CoolParking.WebAPI.Controllers
{
    public class VehicleController : BaseController
    {
        public VehicleController(IParkingService service, ITimerService payTimer, ITimerService logTimer, ILogService logService) : base(service, payTimer, logTimer, logService)
        {
        }

        // GET api/vehicles
        //[Route("~/vehicles")]
        [HttpGet]
        public ActionResult<List<Vehicle>> GetVehicles()
        {
            try
            {
                return Ok(_parkingService.GetVehicles());
            }
            catch (Exception ex)
            { return BadRequest(ex.Message); }
        }

        // GET api/vehicles/id (id - vehicle id of format “AA-0001-AA”)
        //[Route("~/vehicles")]
        [HttpGet("id", Name = "Get")]
        public ActionResult<Vehicle> GetVehicleById(string id)
        {
            var vehicle = _parkingService.GetVehicles().FirstOrDefault(dm => dm.Id == id);
            if (IsMatched(id))
                return BadRequest();
            if (vehicle == null)
            {
                return NotFound();
            }
            return Ok(vehicle);
        }

        private static bool IsMatched(string plate)
        {
            string _pattern = @"^([A-Z]{2}\-\d{4}\-[A-Z]{2}$)";
            return Regex.IsMatch(plate, _pattern);
        }
        private static bool IsValidId(string id)
        {
            return IsMatched(id);
        }

        private static bool IsValidTypeOfVehicle(VehicleType type)
        { 
            return Enum.IsDefined(typeof(VehicleType), type);
        }
        private static bool IsValidBalance(decimal balance)
        {
            return balance > 0;
        }
        private bool IsValidVehicle(Vehicle vehicle)
        {
            return IsValidId(vehicle.Id) && IsValidTypeOfVehicle(vehicle.VehicleType) && IsValidBalance(vehicle.Balance);
        }

        //private VehicleType ValidatingEnum(string type)
        //{
        //    VehicleType vehicleType;
        //    if(Enum.TryParse<VehicleType>(type, out vehicleType))
        //    {
        //        return vehicleType;
        //    }
        //    else
        //    {
        //        return default(VehicleType);
        //    }
        //}

        [HttpPost]
        public ActionResult AddVehicle(Vehicle vehicle)
        {
            try
            {
                _parkingService.AddVehicle(vehicle);
                return CreatedAtRoute("Get", new { id = vehicle.Id }, GetVehicleById(vehicle.Id));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            //decimal decimalbalance = Convert.ToDecimal(balance);
            //if (IsValidVehicle(vehicle))
            //{
            //    _parkingService.AddVehicle(vehicle);
            //    return CreatedAtRoute("Get", new { id = vehicle.Id }, GetVehicleById(vehicle.Id));
            //}
            //else
            //    return BadRequest();
        }

        //DELETE api/vehicles/id(id - vehicle id of format “AA-0001-AA”)
        //[Route("~/vehicles")]
        [HttpDelete("id")]
        public ActionResult DeleteVehicle(string id)
        {
            var vehicle = _parkingService.GetVehicles().FirstOrDefault(dm => dm.Id == id);
            if (IsMatched(id) || vehicle.Balance <= 0)
                return BadRequest();
            if (vehicle == null)
                return NotFound();
            _parkingService.RemoveVehicle(id);
            return NoContent();
        }
    }
}
