﻿using CoolParking.BL.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.IO;
using System.Reflection;

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BaseController : ControllerBase
    {
        protected static IParkingService _parkingService;
        protected static ITimerService _payTimer;
        protected static ITimerService _logTimer;
        protected static ILogService _logService;
        private static string _path = $@"{Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)}\Transactions.log";

        public BaseController(IParkingService service, ITimerService payTimer, ITimerService logTimer, ILogService logService)
        {
            _parkingService = service;
            _payTimer = payTimer;
            _logTimer = logTimer;
            _logService = logService;
        }
    }
}
