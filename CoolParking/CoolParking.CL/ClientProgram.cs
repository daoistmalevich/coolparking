﻿using CoolParking.BL.Models;
using CoolParking.BL.Services;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Http.Json;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CoolParking.CL
{
    public class ClientProgram
    {
        private static HttpClient _client;
        private static ParkingService _parkingService;
        private static string _path = $@"{Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)}\Transactions.log";

        static void Main(string[] args)
        {
            _client = new HttpClient();
            var payTimer = new TimerService();
            var logTimer = new TimerService();
            var logService = new LogService(_path);

            _parkingService = new ParkingService(payTimer, logTimer, logService);
            bool showMenu = true;
            while (showMenu)
            {
                showMenu = MainMenu();
            }
        }

        private static bool MainMenu()
        {
            Console.Clear();
            Console.WriteLine("Choose an option:");
            if (GetServerVehicles() == null)
            {
                Console.WriteLine("1) Get Parking Balance");
                Console.WriteLine("2) Get Parking Capacity");
                Console.WriteLine("3) Show Number Of Free Parking Places");
                Console.WriteLine("6) Put A Vehicle On Parking");
            }
            else
            {
                Console.WriteLine("1) Get Parking Balance");
                Console.WriteLine("2) Get Parking Capacity");
                Console.WriteLine("3) Show Number Of Free Parking Places");
                Console.WriteLine("4) Show List Of Parked Vehicles");
                Console.WriteLine("5) Show Current Vehicle");
                Console.WriteLine("6) Put A Vehicle On Parking");
                Console.WriteLine("7) Remove A Vehicle From Parking");
                Console.WriteLine("8) Show Last Transaction");
                Console.WriteLine("9) Show Transaction Log");
                Console.WriteLine("0) Top Up The Vehicle Balance");
                Console.Write("\r\nSelect an option: ");
            }

            switch (Console.ReadLine())
            {
                case "1":
                    Console.WriteLine("\r\nYou have choosen To: Get Parking Balance");
                    Console.WriteLine($"\r\nCurrent Parking Balance is: {GetServerBalance()}");
                    Console.Write("\r\nPress Enter to return to Main Menu");
                    return true;
                case "2":
                    Console.WriteLine("\r\nYou have choosen To: Get Parking Capacity");
                    Console.WriteLine($"\r\nCurrent Parking Capacity is: {GetServerCapacity()}");
                    return true;
                case "3":
                    Console.WriteLine("\r\nYou have choosen To: Show Number Of Free Parking Places");
                    Console.WriteLine($"\r\nCurrent Number Free Parking Places is: {GetServerFreePlaces()}");

                    return true;
                case "4":
                    Console.WriteLine("\r\nYou have choosen To: Show List Of Parked Vehicles");
                    Console.WriteLine($"\r\nList Of Parked Vehicles is: {GetServerVehicles()}");
                    return true;
                case "5":

                    Console.WriteLine("\r\nYou have choosen To: Put A Vehicle On Parking");
                    Console.WriteLine("\r\nTo Put Vehicle You Need To Register Your Vehicle");
                    var id = GetVehicleId();
                    Console.WriteLine($"\r\nCurrent Vehicle is: {GetServerVehiclesVehicle(id)}");
                    return true;
                case "6":
                    return SetVehicleToAdd();

                case "7":
                    return SetVehicleIdToRemove();
                case "8":
                    Console.WriteLine("\r\nYou have choosen To: Show Last Transaction");
                    Console.WriteLine($"\r\nCurrent Transactions is: {GetLastTransSerever()}");
                    return true;
                case "9":
                    
                    Console.WriteLine("\r\nYou have choosen To: Show All Transaction");
                    Console.WriteLine($"\r\nAll Transactions is: {GetAllTransSerever()}");
                    return true;
                case "0":
                    TopUpCurrentVehicle();
                    return true;
                default:
                    return false;
            }

        }
        private static bool GetVehicleById()
        {
            Console.WriteLine("\r\nYou have choosen To: Put A Vehicle On Parking");
            Console.WriteLine("\r\nTo Put Vehicle You Need To Register Your Vehicle");
            var id = GetVehicleId();
            Console.WriteLine($"\r\nCurrent Vehicle is: {GetServerVehiclesVehicle(id)}");
            return true;
        }
        private static bool SetVehicleToAdd()
        {
            Console.WriteLine("\r\nYou have choosen To: Put A Vehicle On Parking");
            Console.WriteLine("\r\nTo Put Vehicle You Need To Register Your Vehicle");
            var id =GetVehicleId();
            Console.WriteLine("\r\nChoose Your Vehicle Type (0 - PassengerCar, 1 - Truck, 2 - Bus, 3 - Motorcycle) :");

            var type = Console.ReadLine();
            VehicleType vehicle;
            while (!Enum.TryParse(type, false, out vehicle))
            {
                Console.WriteLine("Invalid Vehicle Type, Write correct :");
                type = Console.ReadLine();
            }
            Console.WriteLine("\r\nType Amount Of Balance On Your Vehicle (Should be > 0) : ");
            string curSum = Console.ReadLine();
            decimal balance;
            while (!decimal.TryParse(curSum, out balance) || balance <= 0)
            {
                Console.WriteLine("Invalid Balance Format, Write correct one :");
                curSum = Console.ReadLine();
            }
            AddVehicleServer(new Vehicle(id, vehicle, balance));

            Console.WriteLine($"\r\nThe Vehicle with parameters {id}, {vehicle}, {balance} is Parked");
            return false;
        }
        private static bool SetVehicleIdToRemove()
        {
            Console.WriteLine("\r\nYou have choosen To: Remove A Vehicle From Parking");
            Console.WriteLine("\r\nTo Remove Vehicle You Need To Write Your Vehicle Id");
            var id = GetVehicleId();
            if (GetServerVehiclesVehicle(id).Result.Balance < 0)
            {
                Console.WriteLine("You Cannot Remove Vehicle Until You Pay Your Dept");
                return false;  //Чи вийде з блоку кейс чи ні?
            }
            RemoveVehicleSerever(id);
            Console.WriteLine($"\r\nThe Vehicle with id {id} is Removed From Parking");
            return true;
        }
        private static bool TopUpCurrentVehicle()
        {
            Console.WriteLine("\r\nYou have choosen To: Top Up The Vehicle Balance");
            Console.WriteLine("\r\nTo Top Up Vehicle You Need To Write Your Vehicle Id");
            var id  = GetVehicleId();
            Console.WriteLine("\r\nType Amount Of Cash To Top Up Your Vehicle (Should be > 0) : ");
            string topUpBalance = Console.ReadLine();
            decimal balanceTopUp;
            while (!decimal.TryParse(topUpBalance, out balanceTopUp) || balanceTopUp < 0)
            {
                Console.WriteLine("Invalid Amount Of Cash , Write correct one :");
                topUpBalance = Console.ReadLine();
            }
            TopUpVehicleSerever(id, balanceTopUp);
            Console.WriteLine($"\r\nThe Vehicle with id {id} is top uped on {balanceTopUp} Sum");
            return true;
        }

        private static string GetVehicleId()
        {
            Console.WriteLine("\r\nType Vehicle Id (Should have format \"XX-YYYY-XX\" Where X - Character, Y - Number) : ");
            string plate = Console.ReadLine();
            while (!IsMatched(plate))
            {
                Console.WriteLine("Invalid Vehicle Id Format, Write correct one :");
                plate = Console.ReadLine();
            }
            return plate;
        }
        private static bool IsMatched(string plate)
        {
            string _pattern = @"^([A-Z]{2}\-\d{4}\-[A-Z]{2}$)";
            return Regex.IsMatch(plate, _pattern);
        }

        // parking
        private static async Task<decimal> GetServerBalance()
        {
            var balance = await _client.GetStringAsync("http://localhost:5000/api/parking/balance");
            return JsonConvert.DeserializeObject<decimal>(balance);
        }
        private static async Task<int> GetServerCapacity()
        {
            var capacity = await _client.GetStringAsync("http://localhost:5000/api/parking/capacity");
            return JsonConvert.DeserializeObject<int>(capacity);

        }
        private static async Task<int> GetServerFreePlaces()
        {
            var free = await _client.GetStringAsync("http://localhost:5000/api/parking/freePlaces");
            return JsonConvert.DeserializeObject<int>(free);

        }
        // vehicles
        private static async Task<List<Vehicle>> GetServerVehicles()
        {
            var vehicles = await _client.GetStringAsync("http://localhost:5000/api/vehicles");
            return JsonConvert.DeserializeObject<List<Vehicle>>(vehicles);

        }

        private static async Task<Vehicle> GetServerVehiclesVehicle(string id)
        {
            var vehicle = await _client.GetStringAsync("http://localhost:5000/api/vehicles/{id}");
            return JsonConvert.DeserializeObject<Vehicle>(vehicle);

        }

        private static async void AddVehicleServer(Vehicle vehicle)
        {

            await _client.PostAsJsonAsync("http://localhost:5000/api/vehicles", vehicle);

        }
        private static async void RemoveVehicleSerever(string id)
        {
            await _client.DeleteAsync("http://localhost:5000/api/vehicle/id?id={id}");

        }
        private static async Task<TransactionInfo[]> GetLastTransSerever()
        {
            var lastTranses = await _client.GetStringAsync("http://localhost:5000/api/transactions/last");
            return JsonConvert.DeserializeObject<TransactionInfo[]>(lastTranses);

        }
        private static async Task<string> GetAllTransSerever()
        {
            var allTranses = await _client.GetStringAsync("http://localhost:5000/api/transactions/all");
            return JsonConvert.DeserializeObject<string>(allTranses);

        }
        private static async void TopUpVehicleSerever(string id, decimal sum)
        {
            await _client.PutAsJsonAsync("http://localhost:5000/api/parking/transactions/topUpVehicle?id?={id}", sum);
            
        }
    }
}
