﻿// TODO: implement class Vehicle.
//       Properties: Id (string), VehicleType (VehicleType), Balance (decimal).
//       The format of the identifier is explained in the description of the home task.
//       Id and VehicleType should not be able for changing.
//       The Balance should be able to change only in the CoolParking.BL project.
//       The type of constructor is shown in the tests and the constructor should have a validation, which also is clear from the tests.
//       Static method GenerateRandomRegistrationPlateNumber should return a randomly generated unique identifier.
using Newtonsoft.Json;
using System;
using System.Text.RegularExpressions;

namespace CoolParking.BL.Models
{
    public class Vehicle
    {
        public string Id { get; set; }
        public VehicleType VehicleType { get; set; }
        public decimal Balance { get; set; }
        public static string GenerateRandomRegistrationPlateNumber()
        {
            return $"{GetRandomCharacters(2)}-{GetRandomNumbers(4)}-{GetRandomCharacters(2)}";
        }
        private static string GetRandomCharacters(int length)
        {
            Random random = new Random();

            string rString = "";
            for (var i = 0; i < length; i++)
            {
                rString += ((char)(random.Next(1, 26) + 64)).ToString();
            }
            return rString;
        }
        private static string GetRandomNumbers(int length)
        {
            Random random = new Random();

            string rString = "";
            for (var i = 0; i < length; i++)
            {
                rString += (random.Next(0, 9)).ToString();
            }
            return rString;
        }
        public bool IsMatched(string plate)
        {
            string _pattern = @"^([A-Z]{2}\-\d{4}\-[A-Z]{2}$)";
            return Regex.IsMatch(plate, _pattern);
        }

        public Vehicle (string ID, VehicleType vehicleType, decimal _Balance)
        {
            if (IsMatched(ID))
            {
                Id = ID;
            } else
            {
                throw new ArgumentException();
            }
            VehicleType = vehicleType;
            if (_Balance > 0)
            {
                Balance = _Balance;
            }
            else
            {
                throw new ArgumentException();
            }
        }
    }
}