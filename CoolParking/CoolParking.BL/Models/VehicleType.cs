﻿// TODO: implement enum VehicleType.
//       Items: PassengerCar, Truck, Bus, Motorcycle.
using Newtonsoft.Json.Converters;
using System.Runtime.Serialization;
using System.Text.Json.Serialization;

namespace CoolParking.BL.Models
{
    //[JsonConverter(typeof(StringEnumConverter))]
    public enum VehicleType
    {
        //[EnumMember(Value = "Car")]
        PassengerCar = 1,
        //[EnumMember(Value = "Truck")]
        Truck,
        //[EnumMember(Value = "Bus")]
        Bus,
        //[EnumMember(Value = "Motorcycle")]
        Motorcycle
    }
}