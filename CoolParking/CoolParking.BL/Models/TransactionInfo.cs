﻿// TODO: implement struct TransactionInfo.
//       Necessarily implement the Sum property (decimal) - is used in tests.
//       Other implementation details are up to you, they just have to meet the requirements of the homework.
using System;

namespace CoolParking.BL.Models
{
    public struct TransactionInfo
    {
        public decimal Sum { get; set; }
        public string VehicleId{ get; set; }
        public DateTime TransactionTime { get; set; }

        public TransactionInfo(decimal sum, string vehicleId, DateTime transactionTime)
        {
            Sum = sum;
            VehicleId = vehicleId;
            TransactionTime = transactionTime;
        }
        public override string ToString()
        {
            return $"{TransactionTime} {Sum}  money withdrawn from vehicle with Id='{VehicleId}'";
        }
    }
}