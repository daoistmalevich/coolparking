﻿// TODO: implement the ParkingService class from the IParkingService interface.
//       For try to add a vehicle on full parking InvalidOperationException should be thrown.
//       For try to remove vehicle with a negative balance (debt) InvalidOperationException should be thrown.
//       Other validation rules and constructor format went from tests.
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in ParkingServiceTests you can find the necessary constructor format and validation rules.
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;

namespace CoolParking.BL.Services
{
    public class ParkingService : IParkingService, IDisposable
    {
        readonly ITimerService _payTimer;
        readonly ITimerService _logTimer;
        readonly ILogService _logService;
        private List<TransactionInfo> _transactionInfo;
        private readonly Parking _parking;

        public ParkingService(ITimerService payTimer, ITimerService logTimer, ILogService logService)
        {
            _parking = Parking.GetInstance();
            _payTimer = payTimer;
            _logTimer = logTimer;
            _logService = logService;
            _transactionInfo = new List<TransactionInfo>();

            _logTimer.Interval = Settings.LOGEDPERIOD;
            _payTimer.Interval = Settings.PAYMENTPERIOD;

            _payTimer.Elapsed += _timer_Elapsed;
            _logTimer.Elapsed += _logTimer_Elapsed;
            _payTimer.Start();
            _logTimer.Start();
        }

        private void _logTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            StringBuilder builder = new StringBuilder("");
            foreach (var transactionInfo in _transactionInfo)
            {
                builder.Append(transactionInfo.ToString());
            }
            
            _logService.Write(builder.ToString());
            _transactionInfo = new List<TransactionInfo>();
           
        }

        private void _timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            foreach (var vehicle in _parking.Vehicles)
            {
                decimal vehicleBalance = vehicle.Balance;
                decimal parkingPrice = default;
                switch (vehicle.VehicleType)
                {
                    case VehicleType.PassengerCar:
                        parkingPrice = Settings.VEHICLECARTARIFFS;
                        break;
                    case VehicleType.Bus:
                        parkingPrice = Settings.VEHICLEBUSTARIFFS;
                        break;
                    case VehicleType.Truck:
                        parkingPrice = Settings.VEHICLETRUCKTARIFFS;
                        break;
                    case VehicleType.Motorcycle:
                        parkingPrice = Settings.VEHICLEMOTORCYCLETARIFFS;
                        break;
                    default:
                        break;
                }
                decimal toParkingBalance = default;
                if (vehicleBalance - parkingPrice > 0)
                {
                    toParkingBalance = parkingPrice;
                    vehicle.Balance -= parkingPrice;
                }
                else if (vehicleBalance - parkingPrice < 0)
                {
                    toParkingBalance = parkingPrice + (Math.Abs(vehicleBalance - parkingPrice) * Settings.FINERATE);
                    vehicle.Balance = parkingPrice + (Math.Abs(vehicleBalance - parkingPrice) * Settings.FINERATE);
                }
                _parking.Balance += toParkingBalance;
                _transactionInfo.Add(new TransactionInfo(parkingPrice, vehicle.Id, DateTime.Now));
            }
        }
        public decimal GetAllSum()
        {
            decimal sum = 0;
            foreach (var trans in _transactionInfo)
            {
                sum += trans.Sum;
            }
            return sum;
        }

        public void AddVehicle(Vehicle vehicle)
        {
            if (GetFreePlaces() == 0)
                throw new ArgumentException();
            var vehicleToCheck = _parking.Vehicles.FirstOrDefault(dmtr => dmtr.Id == vehicle.Id);
            if (vehicleToCheck != null)
            {
                throw new ArgumentException();
            }

            _parking.Vehicles.Add(vehicle);
        }

        public void Dispose()
        {
            _parking?.Dispose();
        }

        public decimal GetBalance()
        {

            return _parking.Balance;
        }

        public int GetCapacity()
        {
            return Settings.MAXCAPACITY;
        }

        public int GetFreePlaces()
        {
            return Settings.MAXCAPACITY - _parking.Vehicles.Count;
        }

        public TransactionInfo[] GetLastParkingTransactions()
        {
            return _transactionInfo.ToArray();
        }

        public ReadOnlyCollection<Vehicle> GetVehicles()
        {
            return _parking.Vehicles.AsReadOnly();
        }

        public string ReadFromLog()
        {
            return _logService.Read();
        }

        public void RemoveVehicle(string vehicleId)
        {
            var vehicleToDelete = _parking.Vehicles.FirstOrDefault((dmutr) => dmutr.Id == vehicleId);
            if (vehicleToDelete == null)
            {
                throw new ArgumentException();
            }
            _parking.Vehicles.Remove(vehicleToDelete);
        }

        public void TopUpVehicle(string vehicleId, decimal sum)
        {
            var Vehicle = _parking.Vehicles.FirstOrDefault(dmutr => dmutr.Id == vehicleId);
            if (sum < 0)
            {
                throw new ArgumentException();
            }
            if (Vehicle == null)
            {
                throw new ArgumentException();
            }
            Vehicle.Balance += sum;
        }
        
    }
}