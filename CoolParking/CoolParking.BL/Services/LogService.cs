﻿// TODO: implement the LogService class from the ILogService interface.
//       One explicit requirement - for the read method, if the file is not found, an InvalidOperationException should be thrown
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in LogServiceTests you can find the necessary constructor format.
using CoolParking.BL.Interfaces;
using System;
using System.IO;

namespace CoolParking.BL.Services
{
    public class LogService : ILogService
    {
        public string LogPath { get; }

        public LogService (string logPath)
        {
            LogPath = logPath;
        }
        public LogService()
        {

        }

        public string Read()
        {
            try
            {
                using (var file = new StreamReader(LogPath))
                {
                    return file.ReadToEnd();
                }
            }
            catch
            {
                throw new InvalidOperationException();
            }
        }

        public void Write(string logInfo)
        {
            try
            {
                if (String.IsNullOrEmpty(logInfo))
                {
                    try
                    {
                        using (StreamWriter writer = new StreamWriter(LogPath, true))
                        {
                            writer.WriteLine(logInfo);
                            writer.Close();
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ArgumentException(ex.Message);
                    }
                }
            }
            catch(Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }
    }
}